## sending emails with templates 

using [courier](https://github.com/softprops/courier) and [pureconfig](https://github.com/pureconfig/pureconfig). 

## why?

lets say you want to admonish a lot of people at the same time. 

you have a csv file containing their names, email addresses, and offenses. 

```
mail,name,offense
e-beth@hypermail.com,e-beth,building an e-mail-tool instead of writing her papers
bob.mail.com,bob,not having a viable e-mail address
```

and you have a template for your admonishing email:

```
§§TO:{{mail}}
§§FROM:your@mail.com
§§SUBJECT:admonition
§§CONTENT:hello {{name}},

i am writing to you today because you have commited the offense of {{offense}}.

i am very disappointed in you. 
please refraim from doing such a thing in the future.

best regards.
```

then despair no more! 

## how? 

3 simple steps: 

* go to `main/resources/application.conf`

you will find something that looks like this 

```
smtpserver=smtp-mail.outlook.com
smtpport=587
templatepath=src/main/resources/template
csvpath=src/main/resources/data.csv
```

put your smtp-server and port in there, and change the paths to the template and data files if you so desire. 

* put your csv and template file into the right places. 

**template file** must look pretty much exactly like the example one above. each field has to be marked with `§§` because reasons, and right now, the `TO` field can only contain one (1) single address. please take care.

**csv** must use `,` and `\n` as separators. 

* run the thingy. it will show you the emails it has built and ask you before you it sends anything. 

it will ask you to put your mailing credentials in. i promise not to steal them. (you can just look at the code though.)
