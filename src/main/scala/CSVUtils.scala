
case class CSVData(headings: Vector[String], datarows: List[Vector[String]]) {

  // give me the correct index for a certain heading
  def index      : Map[String, Int] = (headings,headings.indices).zipped.map((_ , _)).toMap

  def rows       : List[Vector[String]] = datarows


  def column(heading: String) : List[String] = {
    //throws exceptions, please watch out
    val i = index(heading)
    rows.map(x => x(i))
  }

  //serialize
  def toCSV(ROW_SEPARATOR : String = "\n", VALUE_SEPARATOR: String = ",") : String = {
    headings.mkString(VALUE_SEPARATOR) + ROW_SEPARATOR +
      datarows.map(x => x.mkString(VALUE_SEPARATOR)).mkString(ROW_SEPARATOR)
  }


}

case class CSVParser(ROW_SEPARATOR : String = "\n", VALUE_SEPARATOR: String = ",") {

  // TODO: Errors, oh errors! Love, oh Love!

  def matrix(str: String) : List[List[String]] = str
    .split(ROW_SEPARATOR)
    .toList
    .map(x => x.trim)
    .map(x => x.split(VALUE_SEPARATOR).toList.map(x => x.trim))

  def apply(str: String) : CSVData = CSVData(matrix(str).head.toVector, matrix(str).tail.map(x => x.toVector))

}