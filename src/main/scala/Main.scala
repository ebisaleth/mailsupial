import javax.security.auth.Subject

import courier._
import Defaults._
import pureconfig._

import scala.concurrent._
import scala.concurrent.duration._
import scala.util.{Failure, Success}


case class MarsuConfig(smtpserver: String, smtpport: Int, templatepath: String, csvpath: String)

object Main extends App {

  val config = loadConfigOrThrow[MarsuConfig]

  def readFile(path: String): String = {
    val source = scala.io.Source.fromFile(path)
    val lines = try source.mkString finally source.close()
    lines
  }

/* ************************************ */
/*   putting csvdata into the template  */
/* ********************************* ****/

  val csv = CSVParser()(readFile(config.csvpath))
  val template = readFile(config.templatepath)

  def insert_in_template(row: Vector[String], template: String): String = {
    var t = template
    csv.headings.foreach({h =>
      t = t.replaceAll(s"\\{\\{$h\\}\\}", row(csv.index(h)))
    })
    t
  }

/* ********************************* */
/*        Making the  E-Mails        */
/* ********************************* */

  // parse the string into a map where "TO:" and "FROM:" and such are keys
  def parse_mail_map(str: String): Map[String, String] = {
    str
      .split("§§")
      .toList.tail
      .map(x => x.trim)
      .map(x => x.split(":",2))
      .map(x => (x(0),x(1)))
      .toMap
  }

  //then, just for safety reasons, pluck it into this case class
  //because a statically typed program is a happy program
  case class EMailWrap(from: String, to: String, subject: String, content: String){


    //...which can envelope itself
    def envelope: Envelope = {
      Envelope.from(from.addr)
        .to(to.addr)
        .subject(subject)
        .content(Text(content))
    }

    //
    override def toString: String = s"FROM: <$from>\nTO: <$to>\nSUBJECT: $subject\nCONTENT: $content"
  }

/* ********************************* */
/*        Boring IO loop foo         */
/*   To make sure everything is OK   */
/*        You can skip ahead         */
/* ********************************* */

  // DO NOT DO THIS AT HOME GUYS. in real life, only an enthusiastic "y" counts as consent.
  def ask_for_consent(): Unit ={
    println("\nis that ok? [y/n]")
    val yn = scala.io.StdIn.readLine()
    if(yn.toLowerCase.trim == "n") {
      println("okay! sorry for the inconvenience. please restart if you want to try again.")
      println("goodbye!")
      System.exit(0)
    }
  }

  // recursion because
  def ask_to_send(c: Int){
    if(c < mails.length){
      Thread.sleep(500)
      println(">>>>>>")
      println(mails(c))
      println("<<<<<<")
      Thread.sleep(500)
    }
    else{
      println("i have already shown you all of the emails!")
    }
    println("\nshould i send them like this? [y=send, n=abort, m=show more emails]")
    val yn = scala.io.StdIn.readLine()
    yn.toLowerCase.trim match {
      case "n" =>
        println("okay! sorry for the inconvenience. please restart if you want to try again.")
        println("goodbye!")
        System.exit(0)
      case "m" => ask_to_send(c+1)
      case "y" =>
      case _ =>
        println("please say 'y' to send, 'n' to abort, or 'm' to see more emails.")
        ask_to_send(c)
    }
  }

  args.headOption match {
    case Some("-v") => debugoutput()
    case _ =>
  }

 def debugoutput(): Unit =  { //-v flag

    println(s"hello. i will show you the template that i found at ${config.templatepath}.\n")
    Thread.sleep(500)
    println(">>>>>>")
    println(template)
    println("<<<<<<")
    Thread.sleep(500)
    ask_for_consent()
    Thread.sleep(500)
    println(s"ok now i will show you the csv i found at ${config.csvpath}.")
    Thread.sleep(500)
    println("this is how it looks to me, so you can see if i parsed it correctly:\n")
    Thread.sleep(500)
    println(">>>>>>")
    println(csv)
    println("<<<<<<")
    Thread.sleep(500)
    ask_for_consent()

  }

  /* ********************************* */
  /*    Putting the things together    */
  /* ********************************* */

  val mails = csv.datarows
    .map(row => insert_in_template(row, template)) // put data in templates
    .map(mail => parse_mail_map(mail)) // make map
    .map(map => EMailWrap(map("FROM"), map("TO"), map("SUBJECT"), map("CONTENT"))) // case class wrapper for typesafety
    .toVector //just for easier access

  println(s"i made ${csv.datarows.length} emails to send for you. i will show you the first one:\n")
  ask_to_send(0)


  /* ********************************* */
  /*         SENDING E-MAILS!!         */
  /* ********************************* */

  //credentials (server and port come from config)

  println(s"alright, so please tell me your login credentials for ${config.smtpserver}. \nfirst your email address/username thingy you log in with: ")
  val id = scala.io.StdIn.readLine()

  println("and now please tell me your password: (yes this is very secure why do you ask)")
  val standardIn = System.console()
  val pwd = standardIn.readPassword().mkString

  //building the mailer thingy

  val mailer = Mailer(config.smtpserver, config.smtpport)
    .auth(true)
    .as(id, pwd)
    .startTtls(true)()

  println("ok. trying to deliver your messages!")

  def envs: List[Envelope] = mails.map(mail => mail.envelope).toList

  def send_with_print(env: Envelope): Future[Unit] = {
    mailer(env).andThen {
      case Success(_) => println(s"message to ${env._to.head} delivered.")
      case Failure(_) => println("message couldn't be delivered.")
    }
  }

  //my smtp does not allow me to send more than one email at a time.
  //this library doesn't do blocking
  //so...
  //this is awkward.

  envs.foreach(e => try {
    Await.result(send_with_print(e),10 seconds)
  } catch {
    case ex: Exception => println(s"sending message to ${e._to} failed: ${ex.getMessage}")
  }
  )

  println("goodbye!")

}
