name := "mailsupial"

version := "1.0"

scalaVersion := "2.12.4"


resolvers += "lightshed-maven" at "http://dl.bintray.com/content/lightshed/maven"

libraryDependencies += "ch.lightshed" %% "courier" % "0.1.4"

//config file
libraryDependencies += "com.github.melrief" %% "pureconfig" % "0.6.0"